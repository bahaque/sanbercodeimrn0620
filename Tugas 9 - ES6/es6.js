console.log(" ")
console.log("1. Arrow Function : ES6")
console.log(" ")

const golden = () => {
  console.log("this is golden!!")
}
golden()


/*
**********  ES 5  **************

const golden = function goldenFunction(){
  console.log("this is golden!!")
}
 
golden()

*/



console.log(" ")
console.log("2. Object Literals : ES6")
console.log(" ")




const firstName1 = "Choridatul",
      lastName1 = "Bahiyyah";

const newFunction = {
  firstName1,
  lastName1,
  fullName() {
    return this.firstName1 +" "+ this.lastName1
  } 
}
console.log(newFunction.fullName())


/*
**********  ES 5  **************

const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

*/




console.log(" ")
console.log("3. Destructuring : ES6")
console.log(" ")


const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject

console.log(firstName, lastName, destination, occupation)


/*
**********  ES 5  **************

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;

// Driver code
console.log(firstName, lastName, destination, occupation)
 
*/




console.log(" ")
console.log("4. Array Spreading : ES6")
console.log(" ")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
west.push.apply(west, east); 

console.log(west);



/*
**********  ES 5  **************

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

//Driver Code
console.log(combined)
 
*/





console.log(" ")
console.log("5. Template Literals : ES6")
console.log(" ")


const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before)



/*
**********  ES 5  **************

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before)
 
*/
