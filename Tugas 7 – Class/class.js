console.log("1. Animal Class")
console.log(" ")

console.log(" ")
console.log("Release 0")
console.log(" ")



class Animal {
    constructor(name){
      this._name = name
      this._legs = '4'
      this._cold_blooded = 'false'
    }
    get namehewan(){
      return this._name
    }
    get kakihewan(){
      return this._legs
    }
    get darahdingin(){
      return this._cold_blooded
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.namehewan)
console.log(sheep.kakihewan)
console.log(sheep.darahdingin)



console.log(" ")
console.log("Release 1")
console.log(" ")



class Ape {
  constructor(name){
    this._name = name
  }
  yell(){
    return "Auooo"
  }
}
class Frog {
  constructor(name){
    this._name = name
  }
  jump(){
    return "hop hop"
  }
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) 
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) 



console.log(" ")
console.log("2. Function to Class")
console.log(" ")


class Clock {
  constructor ({template}){
    this._template = template
  }
  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this._template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this._timer);
  };

  start() {
    this.render();
    this._timer = setInterval(this.render.bind(this), 1000);
  };
}
var clock = new Clock({template: 'h:m:s'});
clock.start();


