console.log("1. if-else");
console.log(" ");

var nama = 'ray'
var peran = 'penyihir'

if(nama == ''){
  console.log("nama harus disi!!!");
    
}else{
    if(peran == 'penyihir'){
      console.log("Selamat datang di Dunia Werewolf, "+ nama);
      console.log("Halo "+ peran +" "+ nama +", kamu dapat melihat siapa yang menjadi werewolf!");
    }
    else if(peran == 'guard'){
      console.log("Selamat datang di Dunia Werewolf, "+ nama);
      console.log("Halo "+ peran +" "+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }
    else if(peran == 'werewolf'){
      console.log("Selamat datang di Dunia Werewolf, "+ nama);
      console.log("Halo "+ peran +" "+ nama +", Kamu akan memakan mangsa setiap malam!");
    }
    else if(peran == ''){
      console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!");
    }
}

console.log(" ");
console.log("2. switch case");
console.log(" ");

var hari =30; 
var bulan = 1; 
var tahun = 2200;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

if(bulan == 2 || bulan == 4 || bulan == 6 || bulan == 8 || bulan == 10 || bulan == 12){
  if(hari >= 1 && hari <= 30){
    if(tahun >= 1900 && tahun <= 2200){
    switch(bulan) {
      case 2:   { console.log(hari+' Pebruari '+tahun); break; }
      case 4:   { console.log(hari+' April '+tahun); break; }
      case 6:   { console.log(hari+' Juni '+tahun); break; }
      case 8:   { console.log(hari+' Agustus '+tahun); break; }
      case 10:   { console.log(hari+' Oktober '+tahun); break; }
      case 12:   { console.log(hari+' Desember '+tahun); break; }
      default:  { console.log('Nothing ...'); }
    }
    }
    else {
       console.log("Masukkan tahun antara 1900 - 2200 ....!");
    }
  }
  else{
    console.log("Masukkan tgl antara 1 - 30 ....!");
  }
}
else if(bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 19 || bulan == 11){
   if(hari >= 1 && hari <= 31){
    if(tahun >= 1900 && tahun <= 2200){
    switch(bulan) {
      case 1:   { console.log(hari+' Januari '+tahun); break; }
      case 3:   { console.log(hari+' Maret '+tahun); break; }
      case 5:   { console.log(hari+' Mei '+tahun); break; }
      case 7:   { console.log(hari+' Juli '+tahun); break; }
      case 9:   { console.log(hari+' September '+tahun); break; }
      case 11:   { console.log(hari+' November '+tahun); break; }
      default:  { console.log('Nothing ...'); }
    }
    }
    else{
       console.log("Masukkan tahun antara 1900 - 2200 ....!");
    }
  }
  else{
    console.log("Masukkan tgl antara 1 - 31 ....!");
  }
}