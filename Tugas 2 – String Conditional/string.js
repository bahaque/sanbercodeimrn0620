// No.1


console.log("1. Membuat kalimat");
console.log(" ");

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';


var s = " ";

console.log(word.concat(s+second).concat(s+third).concat(s+fourth).concat(s+fifth).concat(s+sixth).concat(s+seventh));


// No.2

console.log(" ");
console.log("2. Mengurai kalimat (Akses karakter dalam string)");
console.log(" ");

var sentence = "I am going to be React Native Developer"; 
var exampleFirstWord = sentence[0] ;  
var exampleSecondWord = sentence[2] + sentence[3]  ; 


var thirdWord = sentence[5] + sentence[6]+sentence[7] + sentence[8]+sentence[9];  
var fourthWord = sentence[11] + sentence[12];  
var fifthWord = sentence[14] + sentence[15]; 
var sixthWord = sentence[17] + sentence[18]+sentence[19] + sentence[20]+sentence[21]; 
var seventhWord = sentence[30] + sentence[31]+sentence[32] + sentence[33]+sentence[34]+ sentence[35]+sentence[36] + sentence[37]+sentence[38]; 


// No.3


console.log(" ");
console.log("3. Mengurai Kalimat (Substring)");
console.log(" "); 

var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); 


var secondWord2 = sentence2.substring(4, 15);  
var thirdWord2 = sentence2.substring(15, 17); 
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2 = sentence2.substring(21, 25);


console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2); 


// no.4

console.log(" ");
console.log("4. Mengurai Kalimat dan Menentukan Panjang String");
console.log(" ");

var sentence3 = 'wow JavaScript is so cool'; 
var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 15);  
var thirdWord3 = sentence3.substring(15, 17); 
var fourthWord3 = sentence3.substring(18, 20); 
var fifthWord3 = sentence3.substring(21, 25); 



var firstWordLength = exampleFirstWord3.length;
var secondWord3Length = secondWord3.length;
var thirdWord3Length = thirdWord3.length;
var fourthWord3Length = fourthWord3.length;
var fifthWord3Length = fifthWord3.length;


console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3Length); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3Length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3Length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3Length); 

