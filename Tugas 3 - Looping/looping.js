console.log("1. Looping While");
console.log(" ");

console.log("Looping Pertama");
console.log(" ");

var angka = 2;

while (angka <= 20){
  console.log(angka+" - I love coding");
  angka+=2;
}

console.log(" ");
console.log("Looping Kedua");
console.log(" ");

var angka = 20;

while (angka >= 1){
  console.log(angka+" - I will become a mobile developer");
  angka-=2;
}


console.log(" ");
console.log("2. Looping for");
console.log(" ");

var angka;

for(angka = 1; angka <= 20; angka++){
  if(angka % 2 != 0){
      if(angka % 3 == 0){
        console.log(angka+" - I Love Coding");
      }
      else if(angka % 2 != 0){
        console.log(angka+" - Santai");
      }
  }
  if(angka % 2 == 0){
    console.log(angka+" - Berkualitas");
  }
}



console.log(" ");
console.log("3. Membuat persegi panjang");
console.log(" ");


var panjang = 8
var lebar = 4

function printPersegiPanjang(lebar, panjang){
    for(let i = 0; i < lebar; i++){
      let str = " "
        for(let j = 0; j < panjang; j++){
          str += "# "
        }
        console.log(str)
    }
}
console.log(printPersegiPanjang(lebar, panjang));




console.log(" ");
console.log("4. Membuat tangga");
console.log(" ");


var num = 7

function printTangga(num){
    for(let i = 0; i < num; i++){
      let str = " "
        for(let j = 0; j < (i+1); j++){
          str += "# "
        }
        console.log(str)
    }
}
console.log(printTangga(num));



console.log(" ");
console.log("5. Membuat papan catur");
console.log(" ");



var panjang = 4

// lebar = 1 artinya 2 baris catur
var lebar = 4

function printCatur(lebar, panjang){
    for(let i = 0; i < lebar; i++){
      let str = " "
      let str2 = " "
        for(let j = 0; j < panjang; j++){
          str += " #"
          str2 += "# "
        }
        console.log(str)
        console.log(str2)
    }
}
console.log(printCatur(lebar, panjang));